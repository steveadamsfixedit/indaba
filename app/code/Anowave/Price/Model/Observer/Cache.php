<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\Price\Model\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class Cache implements ObserverInterface
{
	/**
	 * Invalidate Block Output Cache on login 
	 * 
	 * @var Boolean
	 */
	const INVALIDATE_CACHE_ON_LOGIN = false;
	
	/**
	 * @var \Magento\Framework\App\Cache\TypeListInterface
	 */
	protected $cacheTypeList = null;
	
	/**
	 * @var \Magento\Framework\App\Cache\Frontend\Pool
	 */
	protected $cacheFrontendPool = null;
	
	/**
	 * @var \Anowave\Price\Helper\Data
	 */
	protected $helper = null;
	
	/**
	 * Constructor 
	 * 
	 * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
	 * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
	 * @param array $data
	 */
	public function __construct
	(
		\Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
		\Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
		\Anowave\Price\Helper\Data $helper,
		array $data = []
	)
	{
		$this->cacheTypeList 		= $cacheTypeList;
		$this->cacheFrontendPool 	= $cacheFrontendPool;
		
		$this->helper = $helper;
	}
	
	/**
	 * Execute observer 
	 * 
	 * @see \Magento\Framework\Event\ObserverInterface::execute()
	 */
	public function execute(EventObserver $observer)
	{
		if ($this->helper->useInvalidate())
		{
			$this->clearCache();
		}
		
		return true;
	}
	
	/**
	 * Clear cache
	 */
	protected function clearCache()
	{
		foreach (['block_html'] as $type)
		{
			$this->cacheTypeList->invalidate($type);
		}
	
		if (true)
		{
			foreach ($this->cacheFrontendPool as $cacheFrontend)
			{
				$cacheFrontend->getBackend()->clean();
			}
		}
	
		return $this;
	}
}
