<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\Price\Model\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Paypal\Test\Unit\Model\ProTest;

class Price implements ObserverInterface
{
	/**
	 * @var \Anowave\Price\Model\Price
	 */
	protected $priceModel = null;
	
	
	public function __construct
	(
		\Anowave\Price\Model\Price $price
	)
	{
		$this->priceModel = $price;
	}
	
	/**
	 * Execute observer 
	 * 
	 * @see \Magento\Framework\Event\ObserverInterface::execute()
	 */
	public function execute(EventObserver $observer)
	{
		$observer->getProduct()->setFinalPrice
		(
			$this->priceModel->getPrice
			(
				$observer->getProduct(), $observer->getQty()
			)
		);
		
		return true;	
	}
}
