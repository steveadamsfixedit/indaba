<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2017 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Model;

use Magento\Framework\Registry;
use League\CLImate\TerminalObject\Basic\Dump;

class TierPrice extends \Magento\Catalog\Pricing\Price\TierPrice
{
	/**
	 * Get tier price list
	 * 
	 * @see \Magento\Catalog\Pricing\Price\TierPrice::getTierPriceList()
	 */
	public function getTierPriceList()
	{
		if (null === $this->priceList) 
		{
			$priceList = $this->getStoredTierPrices();
			
			$this->priceList = $this->filterTierPrices($priceList);
			
			array_walk($this->priceList,function (&$priceData) 
			{
				/* convert string value to float */
				$priceData['price_qty'] = $priceData['price_qty'] * 1;
				
				/**
				 * Set current quantity
				 */
				$this->product->setAdjustableTierQuantity($priceData['price_qty']);
				
				$priceData['price'] = $this->applyAdjustment($priceData['price']);
			});
		}
		return $this->priceList;
	}
	
	/**
	 * @param AmountInterface $amount
	 * @return float
	 */
	public function getSavePercent(\Magento\Framework\Pricing\Amount\AmountInterface $amount)
	{
		return ceil(100 - ((100 / $this->product->getFinalPrice()) * ($amount->getBaseAmount() + $amount->getTotalAdjustmentAmount())));
	}
}