<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Anowave\Price\Model\Data;

/**
 * Class Country Information
 *
 * @codeCoverageIgnore
 */
class Price extends \Magento\Framework\Api\AbstractExtensibleObject implements \Anowave\Price\Api\Data\PriceArrayInterface
{
	/**
	 * Get price
	 * 
	 * @see \Anowave\Price\Api\Data\PriceArrayInterface::getPrice()
	 */
	public function getPrice()
	{
		return $this->_get('price');
	}
	
	/**
	 * Set price
	 * 
	 * @param float $price
	 * @return $this
	 */
	public function setPrice($price)
	{
		$this->setData('price', $price);
		
		return $this;
	}
	
	public function getPriceCustomerId()
	{
		return $this->_get('price_customer_id');
	}
	
	public function setPriceCustomerId($price_customer_id)
	{
		$this->setData('price_customer_id', $price_customer_id);
		
		return $this;
	}
	
	public function getPriceProductId()
	{
		return $this->_get('price_product_id');
	}
	
	public function setPriceProductId($price_product_id)
	{
		$this->setData('price_product_id', $price_product_id);
	
		return $this;
	}
}
