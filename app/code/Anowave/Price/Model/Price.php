<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Model;

use Magento\Framework\Registry;
use League\CLImate\TerminalObject\Basic\Dump;

class Price
{
	const PRICE_FIXED 		= 0;
	const PRICE_DISCOUNT 	= 1;
	
	/**
	 * Include tax in price calculation 
	 * 
	 * @var Boolean
	 */
	const TAX = false;
	
	/**
	 * \Anowave\Price\Helper\Data
	 */
	protected $helper;
	
	/**
	 * Customer session
	 *
	 * @var \Magento\Customer\Model\Session
	 */
	protected $session = null;
	
	/**
	 * @var \Anowave\Price\Model\ResourceModel\Item\CollectionFactory
	 */
	protected $collectionFactory = null;
	
	/**
	 * @var \Magento\Catalog\Helper\Data
	 */
	protected $catalogData = null;
	
	/**
	 * @var \Magento\Framework\App\Http\Context
	 */
	protected $httpContext = null;
	
	/**
	 * @var \Magento\Framework\Registry
	 */
	protected $registry = null;
	
	/**
	 * @var \Magento\Checkout\Model\Cart
	 */
	protected $cart = null;
	
	/**
	 * Constructor 
	 * 
	 * @param \Magento\Framework\Pricing\Adjustment\CalculatorInterface $calculator
	 * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
	 * @param \Magento\Customer\Model\Session $session
	 * @param \Anowave\Price\Model\ResourceModel\Item\CollectionFactory $collectionFactory
	 * @param \Anowave\Price\Helper\Data $helper
	 * @param \Magento\Catalog\Helper\Data $catalogData
	 * @param \Magento\Framework\App\Http\Context $httpContext
	 */
	public function __construct
	(
		\Magento\Framework\Pricing\Adjustment\CalculatorInterface $calculator,
		\Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
		\Magento\Customer\Model\Session $session,
		\Anowave\Price\Model\ResourceModel\Item\CollectionFactory $collectionFactory,
		\Anowave\Price\Helper\Data $helper,
		\Magento\Catalog\Helper\Data $catalogData,
		\Magento\Framework\App\Http\Context $httpContext,
		\Magento\Framework\Registry $registry,
		\Magento\Checkout\Model\Cart $cart
	)
	{
		$this->helper				= $helper;
		$this->session 				= $session;
		$this->collectionFactory 	= $collectionFactory;
		$this->catalogData			= $catalogData;
		$this->httpContext 			= $httpContext;
		$this->registry				= $registry;
		$this->cart					= $cart;
	}
	
	/**
	 * Check if customer is logged in
	 */
	public function isLoggedIn()
	{
		if ($this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH))
		{
			return true;
		}
		elseif ($this->session->isLoggedIn())
		{
			return true;
		}
		
		return false;
	}
	
	public function getCustomerId()
	{
		if (0 !== (int) $this->registry->registry('cache_session_customer_id'))
		{
			return (int) $this->registry->registry('cache_session_customer_id');	
		}
		else if ($this->session->getCustomerId() > 0)
		{
			return (int) $this->session->getCustomerId();
		}
		
		return 0;
	}
	
	/**
	 * Get Custom price 
	 * 
	 * @param \Magento\Framework\Pricing\SaleableInterface $product
	 * @param number $quantity
	 * @param number $adjustment
	 */
	public function getPrice(\Magento\Framework\Pricing\SaleableInterface $product, $quantity = null, $adjustment = 0)
	{
		/**
		 * Skip processing for bundled product(s)
		 */
		if (\Magento\Catalog\Model\Product\Type::TYPE_BUNDLE == $product->getTypeId())
		{
			return 0;
		}
		
		if (is_null($quantity))
		{
			$quantity = 1;
			
			foreach ($this->cart->getQuote()->getAllItems() as $item)
			{
				if ($item->getProductId() == $product->getId())
				{
					$quantity = $item->getQty();
						
					break;
				}
			}
			
			/**
			 * Get original price
			 *
			 * @var float
			 */
			$value = $this->catalogData->getTaxPrice($product, $product->getPriceInfo()->getPrice('final_price')->getValue(),self::TAX,null,null,null, null,null,false);
		}
		else 
		{
			$value = (float) $this->catalogData->getTaxPrice($product, $product->getPriceInfo()->getPrice('final_price')->getValue(),self::TAX,null,null,null, null,null,false);
			
			if (true)
			{
				$tier = (float) $this->catalogData->getTaxPrice($product, $product->getTierPrice($quantity),self::TAX,null,null,null, null,null,false);
				
				if ($tier < $value)
				{
					$value = $tier;
				}
			}
		}
		
		/**
		 * Get Adjustment
		 */
		if ($adjustment)
		{
			$value = $adjustment;
		}

		if (!$this->helper->legit() || !$this->isLoggedIn())
		{
			return $value;
		}

		/**
		 * Retrieve customer specific price
		 */
		$prices = (array) $this->getCustomerPrice
		(
			$this->getCustomerId(), $product, $quantity
		);
		
		if ($prices)
		{
			$price = end($prices);
			
			switch ($price['price_type'])
			{
				case self::PRICE_FIXED:
						
					$value = $price['price'];
						
					break;
						
				case self::PRICE_DISCOUNT:
						
					$discount = $price['price_discount'];
					$discount = $discount < 0 ? 0 : ($discount > 100 ? 100 : $discount);
		
					$value -= ($value * $discount)/100;

					break;
			}
		}
		
		return $value;
	}
	
	/**
	 * Get customer price 
	 * 
	 * @param unknown $customer
	 * @param \Anowave\Price\Model\Product $product
	 * @param number $quantity
	 */
	protected function getCustomerPrice($customer, \Anowave\Price\Model\Product $product, $quantity = 1)
	{
		$prices = array();
	
		$collection = $this->collectionFactory->create()->addFieldToFilter('price_customer_id',$customer)->addFieldToFilter('price_product_id', $product->getId());
	
		foreach($collection as $item)
		{
			if ((int) $item->getPriceTierQuantity() > 0)
			{
				if ((int) $item->getPriceTierQuantity() > $quantity)
				{
					continue;
				}
			}
		
			$prices[$item->getId()] = array
			(
				'price_product_id'		=> $item->getPriceProductId(),
				'price'					=> $item->getPrice(),
				'price_type' 			=> $item->getPriceType(),
				'price_discount' 		=> $item->getPriceDiscount(),
				'price_tier_quantity' 	=> $item->getPriceTierQuantity(),
				'price_valid_from'		=> $item->getPriceValidFrom(),
				'price_valid_to'		=> $item->getPriceValidTo()
			);
		}
		
		usort($prices, function($a,$b)
		{
			return (float) $a['price_tier_quantity'] >= (float) $b['price_tier_quantity'];
		});
		
		$model = new \Magento\Framework\DataObject(array
		(
			'prices' 	=> $prices,
			'product' 	=> $product,
			'quantity' 	=> $quantity,
			'customer'	=> $customer
		));
	
		/**
		 * Append category prices
		*/
		$this->appendCategoryPrices($model, $collection, $product);
	
		return $this->helper->filter
		(
			$model->getPrices()
		);
	}
	
	public function appendCategoryPrices(\Magento\Framework\DataObject $model, \Anowave\Price\Model\ResourceModel\Item\Collection $collection, $product)
	{
		/**
		 * Get loaded prices array
		 *
		 * @var array
		 */
		$prices = $model->getPrices();
	
	
		/**
		 * Reset collection
		*/
		$collection->clear()->getSelect()->reset('where');
	
	
		/**
		 * Load collection
		*/
		$collection->addCategories()->addFieldToFilter('price_customer_id',$model->getCustomer())->addFieldToFilter('price_category_id', array
		(
			'in' => $product->getCategoryIds()
		));
	
		foreach ($collection as $item)
		{
			$prices[$item->getId()] = array
			(
				'price_product_id'		=> $item->getPriceProductId(),
				'price'					=> $item->getPrice(),
				'price_type' 			=> $item->getPriceType(),
				'price_discount' 		=> $item->getPriceDiscount(),
				'price_tier_quantity' 	=> $item->getPriceTierQuantity(),
				'price_valid_from'		=> $item->getPriceValidFrom(),
				'price_valid_to'		=> $item->getPriceValidTo()
			);
		}
	
		/**
		 * Update model
		 */
		$model->setPrices($prices);
	
		return $this;
	}
}
