<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Model\ResourceModel\Item;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	/**
	 * @var \Magento\Catalog\Model\ProductFactory
	 */
	protected $factory = null;
	
	/**
	 * @var \Magento\Framework\App\RequestInterface
	 */
	protected $request = null;
	
	public function __construct
	(
		\Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
		\Magento\Catalog\Model\ProductFactory $factory,
		\Magento\Framework\View\Element\Context $context
	)
	{
		/**
		 * Set factory 
		 * 
		 * @var \Magento\Catalog\Model\ProductFactory
		 */
		$this->factory = $factory;
		
		/**
		 * Set request 
		 * 
		 * @var \Magento\Framework\App\RequestInterface
		 */
		$this->request = $context->getRequest();
		
		parent::__construct($entityFactory, $context->getLogger(), $fetchStrategy, $context->getEventManager());
	}
    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Anowave\Price\Model\Item', 'Anowave\Price\Model\ResourceModel\Item');
    }
    
    /**
     * Joines templates information
     *
     * @return $this
     */
    public function addCategories()
    {
    	$this->getSelect()->joinLeft
    	(
    		[
    			'categories' => $this->getTable('ae_price_category')
    		],
    			'categories.price_id = main_table.price_id',
    		[
    			'price_category_id'
    		]
    	);
    	
    	$this->getSelect()->where('main_table.price_id IS NOT NULL')->columns('GROUP_CONCAT(price_category_id) as price_category_id')->group('main_table.price_id');
    	
    	$this->_joinedTables['categories'] = true;
    	
    	return $this;
    }
    
    public function addCurrentCustomer()
    {
    	$this->addFieldToFilter('price_customer_id', $this->request->getParam('id'));
    	
    	return $this;
    }
    
    public function addProductNames()
    {
    	$this->getSelect()->joinLeft(['catalog_product_entity_varchar' => $this->getTable('catalog_product_entity_varchar')],"main_table.price_product_id = catalog_product_entity_varchar.entity_id",["value" => "value"]);
    	
    	return $this;
    }
    
    public function getSize()
    {
    	return sizeof( $this->getAllIds());
    }
    
    public function addDistinctCount()
    {
    	return $this;
    }
    
    /**
     * After load processing
     *
     * @return $this
     */
    protected function _afterLoad()
    {
    	parent::_afterLoad();
    	
    	/**
    	 * Associate each row with product
    	 */
        foreach ($this as $item) 
        {
        	if ((int) $item->getPriceProductId())
        	{
	        	$product = $this->factory->create()->load($item->getPriceProductId());
	        	
	            $item->setProduct($product);
        	}
        }

    	return $this;
    }
}