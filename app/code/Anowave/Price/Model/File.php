<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Model;

use Magento\Framework\Filesystem;

class File extends \Magento\Config\Model\Config\Backend\File
{
	/**
	 * The tail part of directory path for uploading
	 */
	const UPLOAD_DIR = '';
	
	/**
	 * Upload max file size in kilobytes
	 *
	 * @var int
	 */
	protected $_maxFileSize = 124000;
	

	/**

	 * @var \Anowave\Price\Model\ItemFactory
	 */
	protected $factory;
	
	/**
	 * @var \Anowave\Price\Model\CategoryFactory
	 */
	protected $factoryCategory;
	
	/**
	 * @var \Anowave\Price\Model\ResourceModel\Item\Collection
	 */
	protected $factoryCollection;
	
	/**
	 * Constructor 
	 * 
	 * @param \Magento\Framework\Model\Context $context
	 * @param \Magento\Framework\Registry $registry
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
	 * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
	 * @param \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory
	 * @param \Magento\Config\Model\Config\Backend\File\RequestData\RequestDataInterface $requestData
	 * @param Filesystem $filesystem
	 * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
	 * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
	 * @param \Anowave\Price\Model\ItemFactory $factory
	 * @param \Anowave\Price\Model\ResourceModel\Item\Collection $factoryCollection
	 * @param \Anowave\Price\Model\CategoryFactory $factoryCategory
	 * @param array $data
	 */
	public function __construct
	(
		\Magento\Framework\Model\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\App\Config\ScopeConfigInterface $config,
		\Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
		\Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
		\Magento\Config\Model\Config\Backend\File\RequestData\RequestDataInterface $requestData,
		Filesystem $filesystem,
		\Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
		\Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
		\Anowave\Price\Model\ItemFactory $factory,
		\Anowave\Price\Model\ResourceModel\Item\CollectionFactory $factoryCollection,
		\Anowave\Price\Model\CategoryFactory $factoryCategory,
		array $data = []
	) 
	{
		parent::__construct($context, $registry, $config, $cacheTypeList, $uploaderFactory, $requestData, $filesystem, $resource, $resourceCollection);
		
		$this->factory 			 = $factory;
		$this->factoryCategory 	 = $factoryCategory;
		$this->factoryCollection = $factoryCollection;
	}
	
	
	/**
	 * Return path to directory for upload file
	 *
	 * @return string
	 */
	protected function _getUploadDir()
	{
		return $this->_mediaDirectory->getAbsolutePath($this->_appendScopeInfo(self::UPLOAD_DIR));
	}
	
	/**
	 * Makes a decision about whether to add info about the scope
	 *
	 * @return boolean
	 */
	protected function _addWhetherScopeInfo()
	{
		return true;
	}
	
	public function beforeSave()
	{
		$value = $this->getValue();
		
		$tmpName = $this->_requestData->getTmpName($this->getPath());
		
		$file = [];
		
		if ($tmpName) 
		{
			$file['tmp_name'] = $tmpName;
			$file['name'] = $this->_requestData->getName($this->getPath());
		} 
		elseif (!empty($value['tmp_name'])) 
		{
			$file['tmp_name'] = $value['tmp_name'];
			$file['name'] = $value['value'];
		}
		
		if (!empty($file)) 
		{
			$uploadDir = $this->_getUploadDir();

			try 
			{
				$uploader = $this->_uploaderFactory->create(['fileId' => $file]);
				$uploader->setAllowedExtensions($this->_getAllowedExtensions());
				$uploader->setAllowRenameFiles(false);
				$uploader->addValidateCallback('size', $this, 'validateMaxSize');
			
				$result = $uploader->save($uploadDir);
			} 
			catch (\Exception $e) 
			{
				throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
			}
		
			$filename = $result['file'];
			
			if ($filename) 
			{
				$fp = fopen($this->_getUploadDir() . DIRECTORY_SEPARATOR . $filename, 'r+');
				
				$data = [];
				
				if ($fp)
				{
					/**
					 * Remove headers
					 */
					fgetcsv($fp);
					
					while($row = fgetcsv($fp))
					{
						$data[] = 
						[
							'price_customer_id'		=> trim($row[0]),
							'price_product_id'		=> trim($row[1]),
							'price_discount'		=> trim($row[2]),
							'price_type'			=> trim($row[3]),
							'price_category_id'		=> array
							(
								(int) trim($row[4])
							)
						];
					}
					
					fclose($fp);
				}
				
				if ($data)
				{
					$customers = array();
					
					foreach ($data as $row)
					{
						$customers[$row['price_customer_id']][] = $row;
					}
					
					/**
					 * Create fixed model
					 * 
					 * @var Closure
					 */
					$f = function($data)
					{
						$model = $this->factory->create();
						
						$model->setData(array
						(
							'price_customer_id' 	=> (int) $data['price_customer_id'],
							'price_product_id' 		=> (int) $data['price_product_id'],
							'price_type' 			=> (int) 0,
							'price' 				=> (float) $data['price_discount'],
							'price_discount' 		=> (int) 0
						));
						
						return $model;
					};
					
					/**
					 * Create percentage model
					 * 
					 * @var Closure
					 */
					$p = function($data)
					{
						$model = $this->factory->create();
						
						$model->setData(array
						(
							'price_customer_id' 	=> (int) $data['price_customer_id'],
							'price_product_id' 		=> (int) $data['price_product_id'],
							'price_type' 			=> (int) 1,
							'price' 				=> (int) 0,
							'price_discount' 		=> (float) $data['price_discount']
						));
						
						return $model;
					};
					
					foreach ($customers as $customer => $entity)
					{
						/**
						 * Remove customer prices 
						 */
						$collection = $this->factoryCollection->create()->addFieldToFilter('price_customer_id', $customer);
						
						foreach ($collection as $row)
						{
							$row->delete();
						}
						
						/**
						 * Import prices
						 */
						foreach ($entity as $data)
						{
							switch ($data['price_type'])
							{
								case 'F': $model = $f($data);
									break;
								case 'P': $model = $p($data);
									break;
							}
							
							/**
							 * Save model
							 */
							$model->save();
							
							$price_id = $model->getId();
							
							if ($price_id)
							{
								$categories = array_filter((array) $data['price_category_id']);
								
								foreach ($categories as $category_id)
								{
									$category = $this->factoryCategory->create();
								
									$category->setData
									(
										array
										(
											'price_id'			=> $price_id,
											'price_category_id' => $category_id
										)
									);
								
									$category->save();
								}
							}
						}
					}
				}
			}
		} 

		$this->setValue('');
	}
}
