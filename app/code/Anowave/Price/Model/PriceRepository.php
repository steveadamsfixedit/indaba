<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Model;

class PriceRepository implements \Anowave\Price\Api\PriceRepositoryInterface
{
	/**
	 * @var \Anowave\Price\Model\Data\PriceFactory
	 */
	protected $priceFactory = null;
	
	/**
	 * @var \Anowave\Price\Model\ItemFactory
	 */
	protected $itemFactory = null;
	
	/**
	 * Constructor 
	 * 
	 * @param \Anowave\Price\Model\Data\PriceFactory $priceFactory
	 */
	public function __construct
	(
		\Anowave\Price\Model\Data\PriceFactory $priceFactory,
		\Anowave\Price\Model\ItemFactory $itemFactory
	) 
	{
		$this->priceFactory = $priceFactory;
		$this->itemFactory 	= $itemFactory;
	}
	
	/**
	 * Get prices
	 * 
	 * @see \Anowave\Price\Api\PriceRepositoryInterface::getPrices()
	 */
	public function getPrices()
	{
		$prices = [];
		
		foreach (range(1,2) as $item)
		{
			$model = $this->priceFactory->create();
			
			
			$model->setPriceCustomerId(1);
			$model->setPriceProductId(2);
			$model->setPrice(20);
			
			$prices[] = $model;
		}
		
		
		
		return $prices;
	}
	
	/**
	 * Create price 
	 * 
	 * @see \Anowave\Price\Api\PriceRepositoryInterface::createPrice()
	 */
	public function createPrice($price_customer_id)
	{
		return json_encode(
		[
			'price_id' => 1
		]);
	}
	
	/**
	 * Create price
	 *
	 * @see \Anowave\Price\Api\PriceRepositoryInterface::createPrice()
	 */
	public function deletePrice($price_customer_id)
	{
		return json_encode(
		[
			'price_id' => 1
		]);
	}
}