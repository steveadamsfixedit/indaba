<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Block\Adminhtml\Edit\Tab\View;

class Price extends \Magento\Backend\Block\Widget\Grid\Extended
{
	protected $_coreRegistry 		= null;
	protected $_collectionFactory 	= null;
	
    public function __construct
    (
    	\Magento\Backend\Block\Template\Context $context,
    	\Magento\Backend\Helper\Data $backendHelper,
    	\Anowave\Price\Model\ResourceModel\Item\CollectionFactory $collectionFactory,
    	\Magento\Framework\Registry $coreRegistry,
    	array $data = []
    ) 
    {
        $this->_coreRegistry 		= $coreRegistry;
        $this->_collectionFactory 	= $collectionFactory;
        
        parent::__construct($context, $backendHelper, $data);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
    	parent::_construct();
    	
    	$this->setId('customer_price_grid');
    	$this->setUseAjax(true);
    }
    
    protected function _prepareCollection()
    {
    	try 
    	{
	    	$collection = $this->_collectionFactory->create()->addCategories()->addCurrentCustomer()->addProductNames()->addDistinctCount();

	    	/**
	    	 * Set collection
	    	 */
	    	$this->setCollection($collection);
	    	
	    	parent::_prepareCollection();
    	}
    	catch (\Exception $e)
    	{
    		
    	}
    	
    	return $this;
    }
    
    /**
     * Prepare grid mass actions
     *
     * @return void
     */
    protected function _prepareMassaction()
    {
    	$this->setMassactionIdField('price_id');
    	$this->getMassactionBlock()->setUseAjax(true);
    	$this->getMassactionBlock()->setFormFieldName('price');
    
    	$this->getMassactionBlock()->addItem
    	(
    		'delete',
    		[
    			'label' => __('Delete'),
    			'url' => $this->getUrl
    			(
    				'price/index/massDelete',['_current' => true]
    			),
    			'confirm' => __('Are you sure?'),
    			'complete' => 'customer_price_gridJsObject.reload()'
    		]
    	);
    }

    /**
     * Prepare columns.
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('value',
        [
        	'header' 	=> __('Product'),
        	'index'		=> 'value',
        	'renderer' 	=> 'Anowave\Price\Block\Adminhtml\Edit\Tab\View\Renderer\Item'
        ]);
        
        $this->addColumn('price_category_id',
        [
        	'header' 	=> __('Categorie(s)'),
        	'index'		=> 'price_category_id',
        	'renderer' 	=> 'Anowave\Price\Block\Adminhtml\Edit\Tab\View\Renderer\Category'
        ]);
      
        $this->addColumn('price',
        [
        	'header' 	=> __('Fixed Price'),
        	'index'		=> 'price',
        	'type' 		=> 'number',
        	'width' 	=> '100px',
        	'renderer' 	=> 'Anowave\Price\Block\Adminhtml\Edit\Tab\View\Renderer\Fixed'
        ]);

        $this->addColumn('price_discount',
        [
        	'header' 	=> __('Discount'),
        	'index'		=> 'price_discount',
        	'type' 		=> 'number',
        	'renderer'  => 'Anowave\Price\Block\Adminhtml\Edit\Tab\View\Renderer\Discount',
        	'width' 	=> '100px'
        ]);
        
        $this->addColumn('price_tier_quantity',
        [
        	'header' 	=> __('Tier'),
        	'index'		=> 'price_tier_quantity',
        	'type' 		=> 'number',
        	'width' 	=> '50'
        ]);
        
        
        
        
        return parent::_prepareColumns();
    }

    /**
     * Get headers visibility
     *
     * @return bool
     *
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getHeadersVisibility()
    {
        return $this->getCollection()->getSize() >= 0;
    }

    /**
     * {@inheritdoc}
     */
    public function getRowUrl($row)
    {
        return null;
    }
}
