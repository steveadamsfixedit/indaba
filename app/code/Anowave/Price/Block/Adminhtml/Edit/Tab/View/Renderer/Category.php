<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Block\Adminhtml\Edit\Tab\View\Renderer;

/**
 * Adminhtml customers wishlist grid item renderer for name/options cell
 */
class Category extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
	/**
	 * Category background icon
	 *  
	 * @var string
	 */
	private $background = 'iVBORw0KGgoAAAANSUhEUgAAABAAAAAOCAYAAAAmL5yKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkIzOUFCNDNBMTA2QzExRTY4MzJERTJBQzM3NEY2NTZCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkIzOUFCNDNCMTA2QzExRTY4MzJERTJBQzM3NEY2NTZCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QjM5QUI0MzgxMDZDMTFFNjgzMkRFMkFDMzc0RjY1NkIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QjM5QUI0MzkxMDZDMTFFNjgzMkRFMkFDMzc0RjY1NkIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7TjjFNAAACEElEQVR42pySv2sUURDHv293b92Lt2ckPzxPRBI0JipioQYhhZBKKxtrBa3Exsq/IYi1WIm9hYVgFRAt1GBlEw8TRSHGS8xd7vbHZd97u+Psu5g7g6A47DC8fTOf+fFG1J6dJa98AkJY6AkhCZZx9NIrgb+II8jC4ZlHDBB5nAnmA/TWD6y8uU3U52y7ZVCqUD13bwfsQLNLJpF8f4BU2oBV5ngfXnUW1fNzzMsRGdvMwIVVwsen03TsylvRBSjBd5KDXbaErPMVKqojXHuBgneQY5RJQKTZXcOfvLsN+1WB4CDdgYz5v4yggjY7XYM3NMXXotuSqYC6FTiDQGr1AZQFpziKwambO46M5KQr0JvzWGoM43P7CNY7FRT3FDC0N0RV7+8HcEC6hSx+z7bFGoD0JjLVxFKzgi/qIibHSjhVsLHWkmgGCT6VruPkNsBCkrekOWCds+ZaN0p8XgxnMX5gAM1EYLWdQgsbh0Z9xPsu9FUguS+knLVhMhvNK9EthB0J1y1AJ5yAR5Dwi3mOzQ/n9AAkM+h4Ayp0eQQ+j6BgLGgYA9yzVCn4g866G0E81ziKeoCJO+9Ebe40mWHv2jv/+DiCymX4RRdJSnAsbqWhQBs1vp0wPoJ+27U/y5PXq/mKoB0l0PVFzIx8Q7T8EtO3Hot/AuyWhYc3aGTsDD4sPMd/AXKZv3+VMn69nwIMANvXE3kYBBPFAAAAAElFTkSuQmCC';
	
	/**
	 * @var \Magento\Catalog\Model\Category 
	 */
	protected $factory = null;
	/**
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct
    (
    	\Magento\Backend\Block\Context $context,
    	\Magento\Catalog\Model\Category $factory,
    	array $data = []
    )
    {
    	parent::_construct($context, $data);
    	
    	$this->factory = $factory;
    }
    /**
     * Renders item product name and its configuration
     *
     * @param \Magento\Catalog\Model\Product\Configuration\Item\ItemInterface|\Magento\Framework\DataObject $item
     * @return string
     */
    public function render(\Magento\Framework\DataObject $item)
    {
    	$categories = array_unique(array_filter(explode
    	(
    		chr(44), $item->getPriceCategoryId()
    	)));
    	
    	
    	if ($categories)
    	{
    		$names = array();
    		
    		foreach ($categories as $id)
    		{
    			$names[] = "<div style='background:url(data:image/png;base64,$this->background) 0 0 no-repeat; padding:0px 0px 0px 20px;'>" . $this->factory->load($id)->getName() . "</div>";
    		}
    		
    		return nl2br(join(PHP_EOL, $names));
    	}
    	else 
    	{
    		return __('Not set');
    	}
    }
}
