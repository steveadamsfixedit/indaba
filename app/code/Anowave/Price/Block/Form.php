<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\Price\Block;

use Magento\Customer\Controller\RegistryConstants;

/**
 * Adminhtml customer groups edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
	protected $locale = null;
	protected $request = null;
	
    public function __construct
    (
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) 
    {  
        parent::__construct($context, $registry, $formFactory, $data);
        
        $this->locale = $context->getLocaleDate();
        
        /**
		 * @var \Magento\Framework\App\RequestInterface
		 */
        $this->request = $context->getRequest();
    }

    /**
     * Prepare form for render
     *
     * @return void
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $form = $this->_formFactory->create();

        $fieldset = $form->addFieldset('base_fieldset', 
        [
        	'legend' => __('Add price')
        ]);
        

        $fieldset->addField
        (
        	'customer_id', 'hidden',
        	[
        		'name' 		=> 'customer_id',
        		'label' 	=> __('Customer'),
        		'title' 	=> __('Customer'),
        		'value'		=> (int) $this->request->getParam('id')
        	]
        );

        $fieldset->addField
        (
        	'price_type', 'select',
        	[
        		'name' 		=> 'price_type',
        		'label' 	=> __('Price type'),
        		'title' 	=> __('Price type'),
        		'note' 		=> __('Select price type'),
        		'values'	=> array
        		(
        			array
        			(
        				'value' => 0,
        				'label' => __('Fixed price')
        			),
        			array
        			(
        				'value' => 1,
        				'label' => '% discount'
        			)
        		)
        	]
        );
        
        $fieldset->addType('products', 'Anowave\Price\Block\Picker');
        
        $fieldset->addField
        (
            'price_product_id', 'products',
            [
                'name' 		=> 'price_product_id',
                'label' 	=> __('Select product(s)'),
                'title' 	=> __('Select product(s)'),
            	'note'		=> __('You can select 1 or more products at once. Do not select any products for category base discounts.'),
                'required' 	=> false
            ]
        );

        /**
         * Add custom type
         */
        $fieldset->addType('categories', 'Magento\Catalog\Block\Adminhtml\Product\Helper\Form\Category');
        	
        $field = $fieldset->addField
        (
        	'price_category_id', 'categories',
        	[
        		'name' 		=> 'price_category_id',
        		'label' 	=> __('Category'),
        		'title' 	=> __('Category'),
        		'note' 		=> __('Apply discount for all products in selected categories.')
        	]
        );
        

        $fieldset->addField
        (
        	'price_fixed', 'text',
        	[
        		'name' 		=> 'price_fixed',
        		'label' 	=> __('Fixed price'),
        		'title' 	=> __('Fixed price'),
        		'note' 		=> __('Set fixed price'),
        		'required' 	=> false
        	]
        );
        
        $fieldset->addField
        (
        	'price_discount', 'text',
        	[
        		'name' 		=> 'price_discount',
        		'label' 	=> __('Discount'),
        		'title' 	=> __('Discount'),
        		'note' 		=> __('Discount in %'),
        		'required' 	=> false
        	]
        );
        
        $fieldset->addField
        (
        	'price_tier_quantity', 'text',
        	[
        		'name' 		=> 'price_tier_quantity',
        		'label' 	=> __('Tier quantity'),
        		'title' 	=> __('Tier quantity'),
        		'note'		=> __('Apply discount only if quantity is equal or higher.'),
        		'required' 	=> false
        	]
        );

        try 
        {
        	$fieldset->addType('datepicker', 'Anowave\Price\Block\Datepicker');
        	
	        $fieldset->addField
	        (
	        	'price_valid_from', 'datepicker',
	        	[
	        		'name' 			=> 'price_valid_from',
	        		'label' 		=> __('Valid from'),
	        		'title' 		=> __('Valid from'),
	        		'date_format' 	=> $this->locale->getDateFormat(\IntlDateFormatter::SHORT),
	        		'input_format' 	=> \Magento\Framework\Stdlib\DateTime::DATE_INTERNAL_FORMAT,
	        		'note'			=> __('Apply discount from date'),
	        		'required' 		=> false
	        	]
	        );
	        
	        $fieldset->addField
	        (
	        	'price_valid_to', 'datepicker',
	        	[
	        		'name' 			=> 'price_valid_to',
	        		'label' 		=> __('Valid to'),
	        		'title' 		=> __('Valid to'),
	        		'date_format' 	=> $this->locale->getDateFormat(\IntlDateFormatter::SHORT),
	        		'input_format' 	=> \Magento\Framework\Stdlib\DateTime::DATE_INTERNAL_FORMAT,
	        		'note'			=> __('Apply discount to date'),
	        		'required' 		=> false
	        	]
	        );
        }
        catch (\Exception $e)
        {
        	
        }
        
        $submit = $fieldset->addField
        (
        	'submitter', 'text',
        	[
        		'name' 		=> 'submitter'
        	]
        );
        
        $submit->setRenderer
        (
        	$this->getLayout()->createBlock('Anowave\Price\Block\Form\Renderer\Submit')
        );
        
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('customer/*/save'));
        $form->setMethod('post');
        $this->setForm($form);
    }
}