<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\Price\Block;

use Magento\Framework\Data\Form\Element\Select;

class Picker extends \Magento\Framework\Data\Form\Element\Select
{
	/**
	 * @var \Magento\Framework\View\Element\BlockFactory
	 */
	protected $layout;
	
	protected $formKey;
	
	/**
	 * Constructor 
	 * 
	 * @param \Magento\Framework\Data\Form\Element\Factory $factoryElement
	 * @param \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection
	 * @param \Magento\Framework\Escaper $escaper
	 * @param \Magento\Framework\View\Element\BlockFactory $blockFactory
	 * @param unknown $data
	 */
    public function __construct
    (
        \Magento\Framework\Data\Form\Element\Factory $factoryElement,
        \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection,
        \Magento\Framework\Escaper $escaper,
    	\Magento\Framework\View\LayoutInterface $layout,
    	\Magento\Framework\Data\Form\FormKey $formKey,
        $data = []
    ) 
    {
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);
        
        $this->layout = $layout;
        $this->formKey = $formKey;
    }
    
    /**
     * Prepares content block
     *
     * @return string
     */
    public function getContentHtml()
    {
    	return $this->layout->createBlock('Anowave\Price\Block\Adminhtml\Edit\Tab\View\Grid')->setId($this->getHtmlId() . '_content')->setElement($this)->toHtml();
    }
    
    public function getElementHtml()
    {
    	return $this->getContentHtml();
    }
}