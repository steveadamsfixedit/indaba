<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\Price\Block\Plugin;

class Category extends \Magento\Catalog\Block\Adminhtml\Product\Helper\Form\Category
{
	/**
	 * Attach category suggest widget initialization
	 *
	 * @return string
	 */
	public function getAfterElementHtml()
	{
		if (!$this->isAllowed()) 
		{
			return '';
		}
		
		return $this->_layout->createBlock('Anowave\Price\Block\Suggest')->setData(array
		(
			'html_id' 				=> $this->getHtmlId(),
			'selector_options' 		=> $this->_jsonEncoder->encode
			(
				$this->_getSelectorOptions()
			)
		))->toHtml();
	}
}