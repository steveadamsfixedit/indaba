<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\Price\Block\Plugin;

class Amount
{
	/**
	 * @var \Anowave\Price\Helper\Data
	 */
	private $helper = null;
	
	/**
	 * Constructor 
	 * 
	 * @param \Anowave\Price\Helper\Data $helper
	 * @param array $data
	 */
	public function __construct
	(
		\Anowave\Price\Helper\Data $helper,
		array $data = []
	)
	{
		$this->helper  = $helper;
	}
	
	/**
	 * After to HTML 
	 * 
	 * @param string $amount
	 * @param string $content
	 */
	public function afterToHtml($amount, $content)
	{	
		if ($this->helper->hidePrice())
		{
			return $this->helper->getConfig('price/general/hide_message');
		}
		else 
		{
			if ($this->helper->useAsyncPriceLoading())
			{
				$content = '<script type="text/template">' . $content . '</script>';
			}
			
			return $content;
		}
	}
}