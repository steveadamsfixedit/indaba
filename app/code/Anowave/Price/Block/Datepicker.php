<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\Price\Block;

class Datepicker extends \Magento\Framework\Data\Form\Element\Date
{
	public function getAfterElementHtml()
	{
		$calendarYearsRange = $this->getYearsRange();
		
		return '<script type="text/javascript">
		            require(["jquery", "mage/calendar"], function($)
		            {
		                    $("#' . $this->getId() . '").calendar(
		                    {
		                        showsTime: 		'  .($this->getTimeFormat() ? 'true' : 'false') .', ' . ($this->getTimeFormat() ? 'timeFormat: "' . $this->getTimeFormat() . '",' : '') . '
		                        dateFormat: 	"' . $this->getDateFormat() . '",
		                        buttonImage: 	"' . $this->getImage() . '",' . ($calendarYearsRange ? 'yearRange: "' . $calendarYearsRange . '",' : '') . ' buttonText: "' . (string) new \Magento\Framework\Phrase('Select Date') . '"
		                    })
		            });
            	</script>';
	}
}