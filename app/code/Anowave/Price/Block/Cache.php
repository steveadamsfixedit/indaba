<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\Price\Block;

class Cache extends \Magento\Backend\Block\Template
{
	/**
	 * @var \Anowave\Price\Helper\Data
	 */
	protected $priceHelper;
	
	/**
	 * Constructor 
	 * 
	 * @param \Magento\Backend\Block\Template\Context $context
	 * @param \Anowave\Price\Helper\Data $priceHelper
	 * @param array $data
	 */
	public function __construct
	(
		\Magento\Backend\Block\Template\Context $context,
		\Anowave\Price\Helper\Data $priceHelper,
		array $data = []
	)
	{
		parent::__construct($context, $data);
		
		$this->priceHelper = $priceHelper;
	}
	
	/**
	 * Prevent block from rendering 
	 * 
	 * @see \Magento\Backend\Block\Template::_toHtml()
	 */
	protected function _toHtml()
	{
		if (!$this->priceHelper->useAsyncPriceLoading())
		{
			return '';
		}
		
		return parent::_toHtml();
	}
}