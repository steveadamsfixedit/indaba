<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */


namespace Anowave\Price\Helper;

use Magento\Store\Model\Store;
use Anowave\Package\Helper\Package;
use Magento\Framework\Registry;
use Anowave\Package\Helper\Base;

class Data extends \Anowave\Package\Helper\Package
{
	/**
	 * Package name
	 * @var string
	 */
	protected $package = 'MAGE2-PPC';
	
	/**
	 * Config path 
	 * @var string
	 */
	protected $config = 'price/general/license';
	
	/**
	 * @var \Magento\Customer\Model\Session
	 */
	protected $session = null;
	
	/**
	 * @var \Magento\Framework\App\Http\Context
	 */
	protected $httpContext;
	
	public function __construct
	(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Framework\App\Http\Context $httpContext,
		\Magento\Customer\Model\Session $session,array $data = [])
	{
		parent::__construct($context);
		
		$this->session 		= $session;
		$this->httpContext 	= $httpContext;
	}
	
	/**
	 * Get store config
	 *
	 * @param string $config
	 */
	public function getConfig($config)
	{
		return $this->_context->getScopeConfig()->getValue($config, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	
	public function hidePrice()
	{
		return !$this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH) && 1 == (int) $this->getConfig('price/general/hide');
	}
	
	/**
	 * Load prices via AJAX call
	 */
	public function useAsyncPriceLoading()
	{
		return 1 === (int) $this->getConfig('price/cache/async');
	}
	
	/**
	 * Invalidate cache on customer login 
	 */
	public function useInvalidate()
	{
		return 1 === (int) $this->getConfig('price/cache/invalidate');
	}
}
