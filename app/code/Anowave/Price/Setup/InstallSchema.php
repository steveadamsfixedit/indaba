<?php 
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
namespace Anowave\Price\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

    	$sql = array();

		$sql[] = "SET foreign_key_checks = 0";
		
		$sql[] = "CREATE TABLE IF NOT EXISTS " . $installer->getTable('ae_price') . " (price_id int(6) NOT NULL AUTO_INCREMENT,price_customer_id int(6) NOT NULL DEFAULT '0',price_product_id int(6) NOT NULL DEFAULT '0',price_type tinyint(1) NOT NULL DEFAULT '1',price decimal(8,2) NOT NULL DEFAULT '0.00',price_discount decimal(8,2) NOT NULL DEFAULT '0.00',price_tier_quantity int(6) NOT NULL DEFAULT '0',price_valid_from timestamp NULL DEFAULT NULL,price_valid_to timestamp NULL DEFAULT NULL,PRIMARY KEY (price_id)) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8";
		$sql[] = "CREATE TABLE IF NOT EXISTS " . $installer->getTable('ae_price_global') . " (price_global_id int(6) NOT NULL AUTO_INCREMENT,price_global_customer_id int(10) unsigned NOT NULL,price_global_discount smallint(3) NOT NULL DEFAULT '0',price_global_valid_from timestamp NULL DEFAULT NULL,price_global_valid_to timestamp NULL DEFAULT NULL,PRIMARY KEY (price_global_id),UNIQUE KEY price_global_customer_id (price_global_customer_id),CONSTRAINT anowave_customerprice_global_ibfk_1 FOREIGN KEY (price_global_customer_id) REFERENCES " . $installer->getTable('customer_entity') . " (entity_id) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
		$sql[] = "CREATE TABLE IF NOT EXISTS " . $installer->getTable('ae_price_category') . " (price_category_primary_id int(6) NOT NULL AUTO_INCREMENT,price_id int(6) NOT NULL,price_category_id int(10) unsigned NOT NULL,PRIMARY KEY (price_category_primary_id),UNIQUE KEY price_id (price_id,price_category_id),KEY price_category_id (price_category_id),CONSTRAINT anowave_customerprice_category_ibfk_1 FOREIGN KEY (price_id) REFERENCES " . $installer->getTable('ae_price') . " (price_id) ON DELETE CASCADE ON UPDATE CASCADE,CONSTRAINT anowave_customerprice_category_ibfk_2 FOREIGN KEY (price_category_id) REFERENCES " . $installer->getTable('catalog_category_entity') . " (entity_id) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
		
		$sql[] = "SET foreign_key_checks = 1";
		
		foreach ($sql as $query)
		{
			$installer->run($query);
		}
		        

        $installer->endSetup();
    }
}


