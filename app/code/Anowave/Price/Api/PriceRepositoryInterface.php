<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
namespace Anowave\Price\Api;

interface PriceRepositoryInterface
{
	/**
     * Get all countries and regions information for the store.
     *
     * @return \Anowave\Price\Api\Data\PriceArrayInterface[]
     */
    public function getPrices();
    
    /**
     * Create price 
     * 
     * @param int $price_customer_id
     * @return string
     */
    public function createPrice($price_customer_id);
    
    /**
     * Delete price
     *
     * @param int $price_customer_id
     * @return string
     */
    public function deletePrice($price_customer_id);
}