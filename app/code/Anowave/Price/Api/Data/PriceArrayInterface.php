<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
namespace Anowave\Price\Api\Data;

interface PriceArrayInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
	/**
     * Get the price
     *
     * @return string
     */
    public function getPrice();
    
    /**
     * Set price 
     * 
     * @param float $price
     * @return $this
     */
    public function setPrice($price);
    
    /**
     * Get price customer id
     * 
     * @return int
     */
    public function getPriceCustomerId();
    
    /**
     * Set price customer id
     * 
     * @param int $price_customer_id
     * @return $this
     */
    public function setPriceCustomerId($price_customer_id);
    
    /**
     * Get price product id 
     * 
     * @return int
     */
    public function getPriceProductId();
    
    /**
     * Set price product id
     * 
     * @param int $price_product_id
     * @return $this
     */
    public function setPriceProductId($price_product_id);
}