<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Controller\Adminhtml\Index;

use Magento\Backend\App\AbstractAction;

class Grid extends \Anowave\Price\Controller\Adminhtml\Index
{
    /**
     * Price Action
     *
     * @return \Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
    	/**
    	 * Create grid block
    	 * 
    	 * @var string
    	 */
    	$block = $this->resultLayoutFactory->create()->getLayout()->createBlock('Anowave\Price\Block\Adminhtml\Edit\Tab\View\Grid');
    	
    	/**
    	 * Set response
    	 */
    	$this->getResponse()->setContent($block->setId('price_product_id_content')->toHtml())->sendResponse();
    	exit();
    }
}