<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Controller\Adminhtml\Index;

class Create extends \Magento\Backend\App\Action
{
	/**
	 * @var \Anowave\Price\Model\ItemFactory
	 */
	protected $factory = null;
	
	
	/**
	 * @var \Anowave\Price\Model\CategoryFactory
	 */
	protected $factoryCategory 	= null;
	
	/**
	 * @var \Magento\Framework\App\Cache\TypeListInterface
	 */
	protected $cacheTypeList = null;
	
	/**
	 * @var \Magento\Framework\App\Cache\Frontend\Pool
	 */
	protected $cacheFrontendPool = null;
	
	/**
	 * Constructor 
	 * 
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Anowave\Price\Model\ItemFactory $factory
	 * @param \Anowave\Price\Model\CategoryFactory $factoryCategory
	 * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
	 * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
	 */
	public function __construct
	(
		\Magento\Backend\App\Action\Context $context,
		\Anowave\Price\Model\ItemFactory $factory,
		\Anowave\Price\Model\CategoryFactory $factoryCategory,
		\Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
		\Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
	)
	{
		parent::__construct($context);
		
		$this->factory 				= $factory;
		$this->factoryCategory		= $factoryCategory;
		$this->cacheTypeList 		= $cacheTypeList;
		$this->cacheFrontendPool 	= $cacheFrontendPool;
	}
	
    public function execute()
    {
    	$response = new \Magento\Framework\DataObject(array
    	(
    		'success' => false
    	));
    	
    	/**
    	 * Function pusher 
    	 * 
    	 * @var Closure
    	 */
    	$f = function($product = null) use ($response)
    	{
    		$model = $this->factory->create();
    		
    		$data = 
    		[
    			'price_customer_id' 	=> 			$this->getRequest()->getParam('customer_id'),
    			'price_type' 			=> (int) 	$this->getRequest()->getParam('price_type'),
    			'price' 				=> (float) 	$this->getRequest()->getParam('price_fixed'),
    			'price_discount' 		=> (float) 	$this->getRequest()->getParam('price_discount'),
    			'price_tier_quantity'	=> (int) 	$this->getRequest()->getParam('price_tier_quantity'),
    			'price_valid_from'		=> strtotime
    			(
    				$this->getRequest()->getParam('price_valid_from')
    			),
    			'price_valid_to'		=> strtotime
    			(
    				$this->getRequest()->getParam('price_valid_to')
    			)
    		];
    		
    		if ($product > 0)
    		{
    			$data['price_product_id'] = (int) $product;
    		}
    		
    		$model->setData($data);
    		
    		try
    		{
    			$model->save();
    			 
    			/**
    			 * Get insert id
    			 *
    			 * @var int
    			*/
    			$price_id = $model->getId();
    			 
    			/**
    			 * Check id model is saved
    			 *
    			*/
    			if ($price_id)
    			{
    				foreach ((array) $this->getRequest()->getParam('price_category_id') as $category_id)
    				{
    					$category = $this->factoryCategory->create();
    					 
    					$category->setData
    					(
    						array
    						(
    							'price_id'			=> $price_id,
    							'price_category_id' => $category_id
    						)
    					);
    					 
    					$category->save();
    				}
    			}
    			 
    			/**
    			 * Set success status
    			 */
    			$response->setSuccess(true);
    		}
    		catch (\Exception $e)
    		{
    			$response->setError($e->getMessage());
    		}
    	};
    	
    	/**
    	 * Get products
    	 *
    	 * @var \ArrayAccess
    	 */
    	$products = [];
    	 
    	/**
    	 * Get products
    	 *
    	 * @var \ArrayAccess
    	*/
    	$products = (array) $this->getRequest()->getParam('price_product_id');
    	 
    	if ($products)
    	{
    		foreach ($products as $product)
    		{
    			$f((int) $product);
    		}
    	}
    	else 
    	{
    		$f();
    	}
    	

    	/**
    	 * Refresh cache and initialize response
    	 */
    	$this->refresh()->getResponse()->setBody(json_encode($response->getData(), JSON_PRETTY_PRINT));
    }
    
    /**
     * Clear cache
     */
    private function refresh()
    {
    	$types = array
    	(
    		'config',
    		'layout',
    		'block_html',
    		'full_page'
    	);
    
    	foreach ($types as $type)
    	{
    		$this->cacheTypeList->cleanType($type);
    	}
    
    	foreach ($this->cacheFrontendPool as $cacheFrontend)
    	{
    		$cacheFrontend->getBackend()->clean();
    	}
    
    	return $this;
    }
}