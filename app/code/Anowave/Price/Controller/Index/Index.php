<?php
/**
 * Anowave Magento 2 Contact Form
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Contact
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
	/**
	 * @var \Magento\Catalog\Model\ProductFactory
	 */
	protected $productCollection = null;
	
	/**
	 * @var \Magento\Framework\Pricing\Helper\Data
	 */
	protected $priceHelper = null;
	
	/**
	 * @var \Anowave\Price\Model\Price
	 */
	protected $priceModel = null;
	
	protected $jsonResultFactory;
	
	/**
	 * Constructor 
	 * 
	 * @param \Magento\Framework\App\Action\Context $context
	 * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection
	 * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
	 */
	public function __construct
	(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection,
		\Magento\Framework\Pricing\Helper\Data $priceHelper,
		\Anowave\Price\Model\Price $priceModel,
		\Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
	)
	{
		parent::__construct($context);
		
		$this->productCollection 	= $productCollection;
		$this->priceHelper 			= $priceHelper;
		$this->priceModel 			= $priceModel;
		$this->jsonResultFactory 	= $jsonResultFactory;
	}

	public function execute()
	{
		$in = [];
		
		foreach ((array) $this->getRequest()->getParam('products') as $id)
		{
			$in[] = (int) $id;
		}
		
		$collection = $this->productCollection->create();
		
		$collection->addAttributeToSelect('price')->addFieldToFilter('entity_id', array('in',$in));
		
		$prices = [];
		
		foreach ($collection as $product)
		{
			$price = $this->priceModel->getPrice($product);
			
			$prices[$product->getId()] = $this->priceHelper->currency($price, true, false);
		}
		
		$result = $this->jsonResultFactory->create();
		
		$result->setData($prices);
		
		return $result;
	}
}