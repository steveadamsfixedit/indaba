<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Pricing;

use Magento\Framework\Pricing\Adjustment\AdjustmentInterface;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Tax\Helper\Data as TaxHelper;
use Zend\Form\View\Helper\Captcha\Dumb;

class Adjustment implements AdjustmentInterface
{
	/**
	 * Adjustment code tax
	 */
	const ADJUSTMENT_CODE = 'customerPrice';
	
	/**
	 * @var TaxHelper
	 */
	protected $taxHelper;
	
	/**
	 * \Magento\Catalog\Helper\Data
	 *
	 * @var CatalogHelper
	 */
	protected $catalogHelper;
	
	/**
	 * @var int|null
	 */
	protected $sortOrder;
	
	/**
	 * @var \Anowave\Price\Model\Price
	 */
	protected $priceModel = null;
	
	/**
	 * @param TaxHelper $taxHelper
	 * @param \Magento\Catalog\Helper\Data $catalogHelper
	 * @param int|null $sortOrder
	 */
	public function __construct
	(
		TaxHelper $taxHelper, 
		\Magento\Catalog\Helper\Data $catalogHelper,
		$sortOrder,
		\Anowave\Price\Model\Price $priceModel
	)
	{
		$this->taxHelper 		= $taxHelper;
		$this->catalogHelper 	= $catalogHelper;
		$this->sortOrder 		= $sortOrder;

		/**
		 * Set custom pricing model 
		 * 
		 * @var \Anowave\Price\Model\Price
		 */
		$this->priceModel = $priceModel;
	}
	
	/**
	 * Get adjustment code
	 *
	 * @return string
	 */
	public function getAdjustmentCode()
	{
		return self::ADJUSTMENT_CODE;
	}
	
	/**
	 * Define if adjustment is included in base price
	 *
	 * @return bool
	 */
	public function isIncludedInBasePrice()
	{
		return false;
	}
	
	/**
	 * Define if adjustment is included in display price
	 *
	 * @return bool
	 */
	public function isIncludedInDisplayPrice()
	{
		return true;
	}
	
	/**
	 * Extract adjustment amount from the given amount value
	 *
	 * @param float $amount
	 * @param SaleableInterface $saleableItem
	 * @param null|array $context
	 * @return float
	 */
	public function extractAdjustment($amount, SaleableInterface $saleableItem, $context = [])
	{
		return $amount - $this->getPrice($saleableItem);
	}
	
	/**
	 * Apply adjustment amount and return result value
	 *
	 * @param float $amount
	 * @param SaleableInterface $saleableItem
	 * @param null|array $context
	 * @return float
	 */
	public function applyAdjustment($amount, SaleableInterface $saleableItem, $context = [])
	{	
		return $this->getPrice($saleableItem, $amount);
	}
	
	/**
	 * Check if adjustment should be excluded from calculations along with the given adjustment
	 *
	 * @param string $adjustmentCode
	 * @return bool
	 */
	public function isExcludedWith($adjustmentCode)
	{
		return $this->getAdjustmentCode() === $adjustmentCode;
	}
	
	/**
	 * Return sort order position
	 *
	 * @return int
	 */
	public function getSortOrder()
	{
		return $this->sortOrder;
	}
	
	/**
	 * Get price
	 * 
	 * @param SaleableInterface $saleableItem
	 * @param number $amount
	 */
	private function getPrice(SaleableInterface $saleableItem, $amount = 0)
	{
		return $this->catalogHelper->getTaxPrice($saleableItem, $this->priceModel->getPrice($saleableItem, $saleableItem->getAdjustableTierQuantity(), $amount), \Anowave\Price\Model\Price::TAX);
	}
}