<?php
/**
 * Anowave Magento 2 Price Per Customer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Price
 * @copyright 	Copyright (c) 2016 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */
 
namespace Anowave\Price\Preference;

class Phrase extends \Magento\Framework\Phrase\Renderer\Translate
{
	/**
	 * Text to swap 
	 * 
	 * @var string
	 */
	const TEXT_TO_SWAP = 'Out of stock'; 
	
	/**
	 * @var \Anowave\Price\Helper\Data
	 */
	protected $priceHelper = null;
	
	public function __construct
	(
        \Magento\Framework\TranslateInterface $translator,
        \Psr\Log\LoggerInterface $logger,
		\Anowave\Price\Helper\Data $priceHelper
    ) 
	{
    	parent::__construct($translator, $logger);
    	
    	$this->priceHelper = $priceHelper;
    }
	
	/**
     * Render source text
     *
     * @param [] $source
     * @param [] $arguments
     * @return string
     * @throws \Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function render(array $source, array $arguments)
    {
    	/**
    	 * Get original text
    	 * 
    	 * @var string
    	 */
    	$text = end($source);
    	
    	if (self::TEXT_TO_SWAP == $text)
    	{
    		if ($this->priceHelper->hidePrice())
    		{
    			return '';
    		}
    	}
    	
    	return parent::render($source, $arguments);
    }
}